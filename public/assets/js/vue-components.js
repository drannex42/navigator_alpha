var dataURL = '/api/v1/notes/get/all';
var router = new VueRouter({
    mode: 'history',
    linkExactActiveClass: 'is-active',
    routes: []
});

const vm = new Vue({
    router,
    el: '#app',
    data: {
        notes: [], // initialize empty array
        searchNotes: [],
        newTagText: '',
        searchText: '',
        currentNote: {
            title: '',
            content: '',
            tags: null
        },
        editor: {
            editor: ClassicEditor,
            editorData: '',
            editorConfig: {
                toolbar: { items: ['heading', '|', 'bold', 'italic', 'link', 'strikethrough', 'underline', 'bulletedList', 'numberedList', 'blockQuote', 'table', 'image', 'undo', 'redo']},
                placeholder: 'Start writing your new note...',
            }
        }
    },
    methods: {
        viewNote: function (d) {
            this.currentNote = d;
            editorData = d.content;

            $(".sidepanelCover").hide();

            this.newTagText = '';
        },
        updateNote: function (data) {
            var self = this;

            $.ajax({
                type: "POST",
                url: '/api/v1/note/post',
                data: { postData: { data } }
            });
        },
        newNoteButton: function () {
            $(".sidepanelCover").hide();
            var data = {
                'title': '',
                'tags': null
            };

            var self = this;

            $.ajax({
                type: "POST",
                url: '/api/v1/note/post',
                data: { postData: { val: data } },
                success: function (d) {
                    console.log('NEW' + JSON.stringify(d));
                    console.log(d.content);
                    self.currentNote = d;
                    self.currentNote.content = '';
                    self.$delete(self.currentNote.tags, 0)
                    self.notes.push(d);
                    $(".fileList").animate({ scrollTop: $('.fileList').prop("scrollHeight") }, 1000);
                }
            });
        },
        updateNoteMeta: function () {
            /* This is where I will update tags, notebooks, and other metaData to currentNote */
            /* Updating Notebooks will also be here, notebooks will be a specialized tag (ex: 'NoteBook-x223-Home') */
        },
        archiveNote: function (val) {
            /* This is the process to archive (not delete) a note */
            var self = this;
            $.ajax({
                type: "POST",
                url: '/api/v1/note/' + val + '/archive',
                success: function (d) {
                    console.log('ARCHIVED');
                    self.currentNote.archived = true;
                    // since the db is handling calls for non-archived notes, on refresh these won't be there. but prior to refresh we will just... hide it (for now).
                    // "why?" because vue does not handle reactivity inside of arrays... this is a caveat to javascript. hopefully vue 3.0 fixes this. If anyone wants to fix this hack, please do. 
                    $('#note-' + val).hide();
                    self.currentNote = [];
                    $(".sidepanelCover").show();
                }
            });

        },
        addNewTag: function () {
            if (!this.currentNote.tags) {
                this.currentNote.tags = [];
            }

            this.currentNote.tags.push({
                title: this.newTagText
            })

            console.log('sent')
            this.newTagText = ''
        },
        removeTag: function (index) {
            this.$delete(this.currentNote.tags, index)
        },
        deleteNote: function () {
            /* call to API to delete a specific notes id */

        },
        search: function (val) {
            var self = this;

            self.searchText = val;

            if (!val) {
                self.searchNotes = self.notes;
                
            } else {
                var fuseOptions = {
                    shouldSort: true,
                    threshold: 0.3,
                    tokenize: true,
                    findAllMatches: true,
                    location: 0,
                    distance: 300,
                    maxPatternLength: 30,
                    minMatchCharLength: 1,
                    keys: [
                        'title',
                        'content',
                        'tags.title'
                    ]
                };

                if (self.searchText[0] == '#') {
                    fuseOptions.keys = ['tags.title']
                }

                var fuse = new Fuse(self.notes, fuseOptions); // "list" is the item array
                var result = fuse.search(self.searchText);

                self.searchNotes = result;
            }
        }
    },
    watch: {
        $route(to, from) {
            var self = this;
            if (to.query.search) {
                console.log("SEARCHED")
                self.search(decodeURI(to.query.search));
                console.log(to)
            } else {
                self.searchText = '';
            }
        },
        currentNote: {
            handler: function (val, oldVal) {
                var self = this
                $.ajax({
                    type: "POST",
                    url: '/api/v1/note/post',
                    data: { postData: { val } },
                    success: function (d) {
                        console.log('CHANGED:' + JSON.stringify(d));
                    }
                });
            },
            deep: true
        },
        notes: {
            handler: function (val, oldval) {
                this.searchNotes = val;
            }
        },

        searchText: {
            handler: function (val, oldVal) {
                this.search(val);
            }
        }
    },
    mounted() { // when the Vue app is booted up, this is run automatically.
        var self = this // create a closure to access component in the callback below
        $.getJSON(dataURL, function (data) {
            self.notes = data;
            self.searchNotes = self.notes;
            next();
        });

        noteURL = this.$route.query.id
        if (noteURL) {
            /* search for note, then add it to currentnote - self.currentNote = */
        }
    }
});

Vue.use(CKEditor);