const { promisify } = require('util');
const crypto = require('crypto');
const nodemailer = require('nodemailer');
const passport = require('passport');
const User = require('../models/User');
const Document = require('../models/Document');
const shortid = require('shortid');

/**
 * GET /api/v1/notes/get/all
 * Home page.
 */
exports.getAllNotesData = (req, res) => {
  Document.find({ "user_id": req.user.user_id, 'archived': false }).sort('date').exec(function (err, notes) {
      res.send(notes)
   });
};

/** 
 * POST /api/v1/note/post 
 * IF note.document_id exists, then update. 
 * ELSE note.document_id does not exust, then create.
 */

exports.postNoteData = (req, res) => {
  var noteData = req.body.postData.val;
  console.log(req.body);


  var query = { 'document_id': noteData.document_id},
    update = { 
      'title': noteData.title,
      'content': noteData.content,
      'tags': noteData.tags,
      'document_id': noteData.document_id || shortid.generate(),
      'user_id': req.user.user_id
     },
    options = { upsert: true, new: true, setDefaultsOnInsert: true };

  // Find the document
  Document.findOneAndUpdate(query, update, options, function (error, result) {
    if (error) return;
    console.log(result);
    res.send(result);
  });
}

/**
 * POST /api/v1/search/tag/:queryl
 * 
 * */

exports.getTaggedFiles = (req, res) => {
  Document.find({ tags: req.query.tag }, function (err, files) {
    res.send(files)
  });
}

exports.getSearchResults = (req, res) => {
  Document.find({ 'user_id': req.user.user_id })
    .exec(function (err, notes) { 
      if (err) { console.log('ERROR:' + err) }

      var index = FlexSearch.create();

      index.add(1, "test");
  
      console.log("Searching:" + req.params.searchQuery)

      index.search("test", function (result) {
        console.log("RESULTS:" + result)
      });

     });
};

/**
 * POST /api/v1/note/:note_id/archive
 * Archives the note, does not permanently remove, if looking to delete from db then use /api/v1/note/:note_id/remove or postNoteRemove
 *
 * */

exports.postNoteArchive = (req, res) => {
  var query = { 'document_id': req.params.document_id, 'user_id': req.user.user_id },
    update = {
      'archived': true
    }

  // Find the document
  Document.findOneAndUpdate(query, update, function (error, result) {
    if (error) return;
    console.log(result);
    res.send(result);
  });
}

exports.postNoteRemove = (req, res) => {
};


exports.postTestData = (req, res) => {
  for (var i = 1; i <= 10000; i++) {

    var query = { 'document_id': 'ajhkjhjhkkjhkhfhk' },
      update = {
        'title': i + ': This is just a majopr test',
        'content': '<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus</p>',
        'tags': [{'title': 'test'}],
        'document_id': shortid.generate(),
        'user_id': 'CmnXLsAtM'
      },
      options = { upsert: true, new: true, setDefaultsOnInsert: true };

    // Find the document
    Document.findOneAndUpdate(query, update, options, function (error, result) {
     console.log('created' + i)
    });
  };
}